import Html exposing (..)
import Html.Events exposing (..)
import Html.Attributes exposing (..)
import Html.App as App

main = 
    App.beginnerProgram {
        model = model, view = view, update = update
    }

type alias Model = 
    {
        todo: String,
        todos: List String
    }

model : Model
model = 
    { 
        todo = "",
        todos = []
    }

stylesheet = 
    let
        tag = "link"
        attrs = [
            attribute "Rel" "stylesheet",
            attribute "property" "stylesheet" ,
            attribute "href" "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
        ]
        children = []
    in
        node tag attrs children
    

type Msg 
    = UpdateTodo String
    |   AddTodo
    |   RemoveAll
    |   RemoveItem String
    |   ClearInput

update : Msg -> Model -> Model
update msg model = 
    case msg of
        UpdateTodo text -> 
            { model | todo = text }
        AddTodo ->
            { model | todos = model.todo :: model.todos }
        RemoveAll ->
            { model | todos = [] }
        RemoveItem text ->
            { model | todos = List.filter (\x -> x /= text) model.todos }
        ClearInput ->
            { model | todo = "" }

todoItem : String -> Html Msg
todoItem todo = 
            li [][
                text todo, button [class "btn btn-warning", onClick (RemoveItem todo)][text "X"]
            ]

todoList : List String -> Html Msg
todoList todos =
    let
        child =
            List.map todoItem todos
    in 
        ul [] child


view model = 
    div [class "container"] [
        stylesheet,
        input [class "form-control", type' "text", onInput UpdateTodo, value model.todo] [],
        button [class "btn btn-success", onClick AddTodo] [text "Add"],
        button [class "btn btn-danger", onClick RemoveAll] [text "Remove all"],
        div [] [ todoList model.todos ]
    ]